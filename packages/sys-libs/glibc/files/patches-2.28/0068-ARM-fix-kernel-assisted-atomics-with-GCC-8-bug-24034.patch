Upstream: yes, taken from release/2.28/master

From 2a8612b278bdb059e2d069b6d72570d7e0dc7aeb Mon Sep 17 00:00:00 2001
From: Aurelien Jarno <aurelien@aurel32.net>
Date: Wed, 2 Jan 2019 18:21:18 +0100
Subject: [PATCH 68/71] ARM: fix kernel assisted atomics with GCC 8 (bug 24034)

The pre-ARMv7 CPUs are missing atomic compare and exchange and/or
barrier instructions. Therefore those are implemented using kernel
assistance, calling a kernel function at a specific address, and passing
the arguments in the r0 to r4 registers. This is done by specifying
registers for local variables. The a_ptr variable is placed in the r2
register and declared with __typeof (mem). According to the GCC
documentation on local register variables, if mem is a constant pointer,
the compiler may substitute the variable with its initializer in asm
statements, which may cause the corresponding operand to appear in a
different register.

This happens in __libc_start_main with the pointer to the thread counter
for static binaries (but not the shared ones):

  # ifdef SHARED
        unsigned int *ptr = __libc_pthread_functions.ptr_nthreads;
  #  ifdef PTR_DEMANGLE
        PTR_DEMANGLE (ptr);
  #  endif
  # else
        extern unsigned int __nptl_nthreads __attribute ((weak));
        unsigned int *const ptr = &__nptl_nthreads;
  # endif

This causes static binaries using threads to crash when the GNU libc is
built with GCC 8 and most notably tst-cancel21-static.

To fix that, use the same trick than for the volatile qualifier,
defining a_ptr as a union.

Changelog:
	[BZ #24034]
	* sysdeps/unix/sysv/linux/arm/atomic-machine.h
	(__arm_assisted_compare_and_exchange_val_32_acq): Use uint32_t rather
	than __typeof (...) for the a_ptr variable.
(cherry picked from commit fe20bb1d6084bbf38fef587b0fb33eb6257cc1ed)
---
 ChangeLog                                    |  7 +++++++
 NEWS                                         |  1 +
 sysdeps/unix/sysv/linux/arm/atomic-machine.h | 18 +++++++++++++-----
 3 files changed, 21 insertions(+), 5 deletions(-)

diff --git a/ChangeLog b/ChangeLog
index 967050e2c9..54b37a6ccf 100644
--- a/ChangeLog
+++ b/ChangeLog
@@ -1,3 +1,10 @@
+2019-01-02  Aurelien Jarno  <aurelien@aurel32.net>
+
+	[BZ #24034]
+	* sysdeps/unix/sysv/linux/arm/atomic-machine.h
+	(__arm_assisted_compare_and_exchange_val_32_acq): Use uint32_t rather
+	than __typeof (...) for the a_ptr variable.
+
 2018-12-31  H.J. Lu  <hongjiu.lu@intel.com>
 
 	[BZ #24022]
diff --git a/NEWS b/NEWS
index 388ac09060..13b8795e6e 100644
--- a/NEWS
+++ b/NEWS
@@ -31,6 +31,7 @@ The following bugs are resolved with this release:
   [24018] gettext may return NULL
   [24022] riscv may lack <asm/syscalls.h>
   [24027] malloc: Integer overflow in realloc
+  [24034] tst-cancel21-static fails with SIGBUS on pre-ARMv7 when using GCC 8
 
 Security related changes:
 
diff --git a/sysdeps/unix/sysv/linux/arm/atomic-machine.h b/sysdeps/unix/sysv/linux/arm/atomic-machine.h
index ad165274d8..a5d244e65e 100644
--- a/sysdeps/unix/sysv/linux/arm/atomic-machine.h
+++ b/sysdeps/unix/sysv/linux/arm/atomic-machine.h
@@ -49,16 +49,23 @@
    declarations of A_OLDVAL et al because when NEWVAL or OLDVAL is of the
    form *PTR and PTR has a 'volatile ... *' type, then __typeof (*PTR) has
    a 'volatile ...' type and this triggers -Wvolatile-register-var to
-   complain about 'register volatile ... asm ("reg")'.  */
+   complain about 'register volatile ... asm ("reg")'.
+
+   We use the same union trick in the declaration of A_PTR because when
+   MEM is of the from *PTR and PTR has a 'const ... *' type, then __typeof
+   (*PTR) has a 'const ...' type and this enables the compiler to substitute
+   the variable with its initializer in asm statements, which may cause the
+   corresponding operand to appear in a different register.  */
 #ifdef __thumb2__
 /* Thumb-2 has ldrex/strex.  However it does not have barrier instructions,
    so we still need to use the kernel helper.  */
 # define __arm_assisted_compare_and_exchange_val_32_acq(mem, newval, oldval) \
-  ({ union { __typeof (oldval) a; uint32_t v; } oldval_arg = { .a = (oldval) };\
+  ({ union { __typeof (mem) a; uint32_t v; } mem_arg = { .a = (mem) };       \
+     union { __typeof (oldval) a; uint32_t v; } oldval_arg = { .a = (oldval) };\
      union { __typeof (newval) a; uint32_t v; } newval_arg = { .a = (newval) };\
      register uint32_t a_oldval asm ("r0");				      \
      register uint32_t a_newval asm ("r1") = newval_arg.v;		      \
-     register __typeof (mem) a_ptr asm ("r2") = (mem);			      \
+     register uint32_t a_ptr asm ("r2") = mem_arg.v;			      \
      register uint32_t a_tmp asm ("r3");				      \
      register uint32_t a_oldval2 asm ("r4") = oldval_arg.v;		      \
      __asm__ __volatile__						      \
@@ -79,11 +86,12 @@
      (__typeof (oldval)) a_tmp; })
 #else
 # define __arm_assisted_compare_and_exchange_val_32_acq(mem, newval, oldval) \
-  ({ union { __typeof (oldval) a; uint32_t v; } oldval_arg = { .a = (oldval) };\
+  ({ union { __typeof (mem) a; uint32_t v; } mem_arg = { .a = (mem) };       \
+     union { __typeof (oldval) a; uint32_t v; } oldval_arg = { .a = (oldval) };\
      union { __typeof (newval) a; uint32_t v; } newval_arg = { .a = (newval) };\
      register uint32_t a_oldval asm ("r0");				      \
      register uint32_t a_newval asm ("r1") = newval_arg.v;		      \
-     register __typeof (mem) a_ptr asm ("r2") = (mem);			      \
+     register uint32_t a_ptr asm ("r2") = mem_arg.v;			      \
      register uint32_t a_tmp asm ("r3");				      \
      register uint32_t a_oldval2 asm ("r4") = oldval_arg.v;		      \
      __asm__ __volatile__						      \
-- 
2.20.1

