# Copyright 2008-2011 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require gnu [ suffix=tar.xz ] alternatives

SUMMARY="Tools to deal with shar archives"
DESCRIPTION="
The GNU shar utilities -- shar, unshar, uuencode, uudecode, and remsync -- are
a set of tools for encoding and decoding packages of files (in binary or text
format) in a special plain text format called shell archives (shar). This
format can be sent through e-mail (which can be problematic for regular binary
files). The shar utility supports a wide range of capabilities (compressing,
uuencoding, splitting long files for multi-part mailings, providing checksums),
which make it very flexible at creating shar files. Unshar automatically strips
off mail headers and introductory text and then unpacks shar files.
"

LICENCES="GPL-3 public-domain"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    ( linguas: bg ca cs da de el eo es et fi fr ga gl hu id it ja nl pl pt_BR ru sr sv tr uk vi
               zh_CN zh_TW )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.3]
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-4.14.2-Pass-compilation-with-Werror-format-security.patch
    "${FILES}"/${PN}-4.15.2-CVE-2018-1000097.patch
    "${FILES}"/${PN}-4.15.2-fflush-adjust-to-glibc-2.28-libio.h-removal.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=( --enable-nls )

src_install() {
    default

    alternatives_for uuencode ${PN} 1 /usr/$(exhost --target)/bin/uuencode uuencode-${PN}
    alternatives_for uudecode ${PN} 1 /usr/$(exhost --target)/bin/uudecode uudecode-${PN}
}

