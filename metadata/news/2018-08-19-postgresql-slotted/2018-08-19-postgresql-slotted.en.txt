Title: PostgreSQL slotted and client/server split
Author: Thomas Berger <loki@lokis-chaos.de>
Content-Type: text/plain
Posted: 2018-08-19
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: dev-db/postgresql

dev-db/postgresql was split into
- dev-db/postgresql
- dev-db/postgresql-client

The server package dev-db/postgresql is now slotted. Multiple major versions
could be installed at the same time. Future upgrades to newer PostgreSQL versions
can be done with pg_upgrade and it is no longer required to dump and import databases.

If you don't need the postgresql server package, you can use the following command to remove
it:

    cave resolve '!dev-db/postgresql' 'dev-db/postgresql-client' -D dev-db/postgresql
